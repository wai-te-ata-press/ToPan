README: ToPan in docker
=======================

The intent of the docker image is to provide a machine with all ToPan's operating system and R dependencies preinstalled.

## Get the docker image

Either pull a docker image from the gitlab registry at https://gitlab.com/wai-te-ata-press/ToPan/container_registry:
```
docker pull registry.gitlab.com/wai-te-ata-press/topan
```

Or build a docker image on your workstation from this directory/context (NB this is slow):
```
docker build -t topan https://gitlab.com/wai-te-ata-press/ToPan.git#master:docker
```

## Run the docker image

Either run a foreground docker image for command line access, R, CI/CD etc:
```
docker run -it --name topan registry.gitlab.com/wai-te-ata-press/topan /bin/bash
git clone --depth 1 https://gitlab.com/wai-te-ata-press/ToPan.git
cd ToPan
```
Or run RStudio as a web server in a detached docker image on your workstation
```
docker run -d -e PASSWORD=friend --name topan -p 8787:8787 registry.gitlab.com/wai-te-ata-press/topan /init
docker ps 
```
Browse to http://localhost  
Enter username rstudio password friend at the "Sign in to RStudio" page.  
You can use the menu sequence "File > New Project > Version Control > Git" to create a new project by cloning the git repository from URL https://gitlab.com/wai-te-ata-press/ToPan.git  
Then just Open File app.R, and press the green Run App button. 

## More
See "Using the RStudio image" at https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image

